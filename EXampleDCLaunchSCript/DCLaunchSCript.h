//
//  DCLaunchSCript.h
//  EXampleDCLaunchSCript
//
//  Created by Diego Caridei on 27/04/14.
//  Copyright (c) 2014 com.diegocaridei.iProg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCLaunchSCript : NSObject{
    NSTask *task;
    NSPipe *outputPipe;
}
-(NSString*)Output:(NSString*)string;
-(void)stopTask;
-(void)runScriptFromFile:(NSArray*)arguments setText:(NSTextView *)textView :(NSString*)type;
-(NSString *)controll:(NSString*)type;

@end


