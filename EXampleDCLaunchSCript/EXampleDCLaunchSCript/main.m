//
//  main.m
//  EXampleDCLaunchSCript
//
//  Created by Diego Caridei on 27/04/14.
//  Copyright (c) 2014 com.diegocaridei.iProg. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
