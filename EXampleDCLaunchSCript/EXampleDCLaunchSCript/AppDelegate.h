//
//  AppDelegate.h
//  DCTaskExample
//
//  Created by Diego Caridei on 26/04/14.
//  Copyright (c) 2014 com.diegocaridei.iProg. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DCLaunchSCript.h"
@interface AppDelegate : NSObject <NSApplicationDelegate>{
    DCLaunchSCript *task;
}
@property (assign) IBOutlet NSWindow *window;
@property(assign)IBOutlet NSTextView *textView;
@end
