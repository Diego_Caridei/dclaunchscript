//
//  AppDelegate.m
//  DCTaskExample
//
//  Created by Diego Caridei on 26/04/14.
//  Copyright (c) 2014 com.diegocaridei.iProg. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    task=[[DCLaunchSCript alloc]init];
    NSArray *arrayPerl=[NSArray arrayWithObjects:[[NSBundle mainBundle] pathForResource:@"Test" ofType:@"pl"], nil];
    
    [task runScriptFromFile:arrayPerl setText:_textView :@"perl"];
   
  /*
    NSArray *arrayBash=[NSArray arrayWithObjects:[[NSBundle mainBundle] pathForResource:@"Test" ofType:@"sh"],@"hello world", nil];
    
    [task runScriptFromFile:arrayBash setText:_textView :@"bash"];
    
*/
    
}

@end
