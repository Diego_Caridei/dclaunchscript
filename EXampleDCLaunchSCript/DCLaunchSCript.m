//
//  DCLaunchSCript.m
//  EXampleDCLaunchSCript
//
//  Created by Diego Caridei on 27/04/14.
//  Copyright (c) 2014 com.diegocaridei.iProg. All rights reserved.
//

#import "DCLaunchSCript.h"

@implementation DCLaunchSCript

-(void)stopTask{
    [task terminate];
}

-(NSString *)controll:(NSString*)type{
    if ([type isEqualTo:@"bash"]) {
        return @"/bin/bash";
    }
    if ([type isEqualTo:@"python"]) {
        return @"/usr/bin/python";
    }
    else if ([type isEqualTo:@"perl"]){
        return @"/usr/bin/perl";
    }
    else{
        return @"Error type";
    }
}





-(void)runScriptFromFile:(NSArray*)arguments setText:(NSTextView *)textView :(NSString*)type {
    
    NSString *path=[self controll:type];
    dispatch_queue_t taskQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(taskQueue, ^{
        
        @try {
            
            
            task  = [[NSTask alloc] init];
            [task setLaunchPath:path];
            [task setArguments:arguments];
            
            outputPipe = [[NSPipe alloc] init];
            task.standardOutput = outputPipe;
            
            [[outputPipe fileHandleForReading] waitForDataInBackgroundAndNotify];
            [[NSNotificationCenter defaultCenter] addObserverForName:NSFileHandleDataAvailableNotification
                                                              object:[outputPipe fileHandleForReading]
                                                               queue:nil usingBlock:^(NSNotification *notification)
             {
                 NSData *output = [[outputPipe fileHandleForReading] availableData];
                 NSString *outStr = [[NSString alloc] initWithData:output encoding:NSUTF8StringEncoding];
                 
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     
                     
                     
                     textView.string = [textView.string stringByAppendingString:[NSString stringWithFormat:@"\n%@", outStr]];
                     NSRange range;
                     range = NSMakeRange([textView.string length], 0);
                     [textView scrollRangeToVisible:range];
                    // NSLog(@"%@",outStr);
                     [self Output:outStr];
                     
                 });
                 [[outputPipe fileHandleForReading] waitForDataInBackgroundAndNotify];
             }];
            
            [task launch];
            [task waitUntilExit];
        }
        @catch (NSException *exception) {
            NSLog(@"Problem With Task: %@", [exception description]);
        }
        
    });
}
-(NSString*)Output:(NSString*)string{
    return string;
}


@end
